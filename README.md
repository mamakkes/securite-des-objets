# Script d'Interaction avec un Dispositif BLE

Ce script Python permet la communication avec un dispositif Bluetooth Low Energy (BLE), spécifiquement pour lire et écrire des caractéristiques, lister les fichiers sur le dispositif, et extraire des fichiers. Il utilise la bibliothèque `bleak` pour se connecter à un dispositif BLE spécifié, interagir avec ses caractéristiques, et effectuer des opérations sur les fichiers tels que les lister et les extraire du dispositif.

## Fonctionnalités

- Connexion à un dispositif BLE en utilisant son adresse.
- Lecture et écriture de caractéristiques.
- Listing de tous les fichiers stockés sur le dispositif.
- Extraction de fichiers du dispositif.
- Utilisation de tuples nommés pour la gestion structurée des informations de fichier.

## Prérequis

- Python 3.10+
- bleak
- asyncio
- collections
- struct

## Installation

Assurez-vous d'avoir Python 3.10 ou une version supérieure installée sur votre système. Vous pouvez installer les dépendances nécessaires en exécutant :

```bash
pip install bleak
```

## Utilisation

Avant d'exécuter le script, remplacez `DEVICE_ADDRESS` par l'adresse de votre dispositif BLE.

```python
DEVICE_ADDRESS = "DD:A1:81:72:F7:F6"
```

Pour exécuter le script, lancez la commande suivante dans votre terminal :

```bash
python3 Program.py
```


### Aperçu des Fonctionnalités

- **Connexion au Dispositif** : Établit une connexion avec le dispositif BLE en utilisant son adresse.
- **Lecture et Écriture de Caractéristiques** : Permet de lire et d'écrire sur des caractéristiques spécifiées du dispositif BLE.
- **Lister et Extraire des Fichiers** : Liste tous les fichiers stockés sur le dispositif et fournit une fonctionnalité pour les extraire.


## Licence

Ce script est un logiciel open source sous licence MIT.