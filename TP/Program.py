import asyncio
from bleak import BleakClient
from collections import namedtuple
import struct

DEVICE_ADDRESS = "DD:A1:81:72:F7:F6"

CHARACTERISTIC_NAMES = {
    "1b0d1201-a720-f7e9-46b6-31b601c4fca1": "TRIP",
    "1b0d1202-a720-f7e9-46b6-31b601c4fca1": "TEMP",
    "1b0d1203-a720-f7e9-46b6-31b601c4fca1": "BATT",
    "1b0d1204-a720-f7e9-46b6-31b601c4fca1": "SOLAR",
    "1b0d1205-a720-f7e9-46b6-31b601c4fca1": "NJR",
    "1b0d1206-a720-f7e9-46b6-31b601c4fca1": "STATUS",
    "1b0d1207-a720-f7e9-46b6-31b601c4fca1": "GPS",
    "1b0d1208-a720-f7e9-46b6-31b601c4fca1": "RESET",
    "1b0d1301-a720-f7e9-46b6-31b601c4fca1": "MEM",
    "1b0d1302-a720-f7e9-46b6-31b601c4fca1": "LIST",
    "1b0d1303-a720-f7e9-46b6-31b601c4fca1": "NUM",
    "1b0d1304-a720-f7e9-46b6-31b601c4fca1": "READ",
    "1b0d1305-a720-f7e9-46b6-31b601c4fca1": "SEND",
    "1b0d1306-a720-f7e9-46b6-31b601c4fca1": "DEL",
    "1b0d1401-a720-f7e9-46b6-31b601c4fca1": "MODE",
    "1b0d1402-a720-f7e9-46b6-31b601c4fca1": "FW",
    "1b0d1403-a720-f7e9-46b6-31b601c4fca1": "BL",
    "1b0d1404-a720-f7e9-46b6-31b601c4fca1": "RESET",
    "1b0d1405-a720-f7e9-46b6-31b601c4fca1": "STATE",
    "1b0d1406-a720-f7e9-46b6-31b601c4fca1": "TIME",
    "1b0d1407-a720-f7e9-46b6-31b601c4fca1": "ACK",
    "1b0d1408-a720-f7e9-46b6-31b601c4fca1": "PAUSE",
    "1b0d1409-a720-f7e9-46b6-31b601c4fca1": "USER ID",
    "1b0d140a-a720-f7e9-46b6-31b601c4fca1": "LOG",
    "1b0d140b-a720-f7e9-46b6-31b601c4fca1": "EVENT",
    "00001503-0000-1000-8000-00805f9b34fb": "NAME",
    "00001504-0000-1000-8000-00805f9b34fb": "COML",
    "00001505-0000-1000-8000-00805f9b34fb": "COMH",
    "00001506-0000-1000-8000-00805f9b34fb": "HWVER",
}

received_file_data = []
file_info_dict = {}
read_data = {}
hex_values_to_write = []

FsFileInfo = namedtuple('FsFileInfo', ['name', 'size', 'hash', 'creationDate'])


def parse_fs_file_info(data):
    if len(data) != 24:
        raise ValueError(f"Data does not match the expected format length. Expected 24, got {len(data)}")

    name = data[:8].decode('ascii')

    size = struct.unpack('<I', data[8:12])[0]

    hash_value = struct.unpack('<I', data[12:16])[0]
    hash_hex = f"0x{hash_value:x}"

    creation_date = struct.unpack('<Q', data[16:])[0]

    return FsFileInfo(name=name, size=size, hash=hash_hex, creationDate=creation_date)


file_request_format = '<8sII'


def create_file_request(name, chunkId, numChunk):
    if isinstance(name, str):
        name_bytes = name.encode('ascii').ljust(8, b'\x00')[:8]
    elif isinstance(name, bytearray) or isinstance(name, bytes):
        name_bytes = name.ljust(8, b'\x00')[:8]
    else:
        raise ValueError("Name must be a string or bytearray/bytes.")

    file_request_data = struct.pack(file_request_format, name_bytes, chunkId, numChunk)

    return file_request_data


def list_notification_handler(sender, data):
    print(f"LIST Notification from {sender}: {data}")
    received_file_data.append(data)


def display_parsed_file_info():
    for data in received_file_data:
        try:
            file_info = parse_fs_file_info(data)
            print(file_info)
            # Store the name (cleaned of null bytes) and size in the dictionary
            file_info_dict[file_info.name.rstrip('\x00')] = file_info.size
        except ValueError as e:
            print(e)


async def list_all_files(client):
    num_char_uuid = "1b0d1303-a720-f7e9-46b6-31b601c4fca1"
    list_char_uuid = "1b0d1302-a720-f7e9-46b6-31b601c4fca1"

    num_files_data = await client.read_gatt_char(num_char_uuid)
    num_files = int.from_bytes(num_files_data, byteorder='little')
    print(f"Number of files: {num_files}")

    await client.start_notify(list_char_uuid, list_notification_handler)
    print("Subscribed to LIST notifications.")

    value_to_write = num_files.to_bytes(2, byteorder='little')
    print(f"Writing to LIST: {value_to_write.hex()}")
    await client.write_gatt_char(list_char_uuid, value_to_write)
    print("Requested file listing.")

    await asyncio.sleep(3)

    await client.stop_notify(list_char_uuid)
    print("Unsubscribed from LIST notifications.")


def process_files_and_create_requests():
    global hex_values_to_write
    hex_values_to_write.clear()
    for name, size in file_info_dict.items():
        chunkId = 0
        numChunk = size
        file_request_data = create_file_request(name, chunkId, numChunk)
        hex_values_to_write.append((file_request_data, name))
        print(f"Prepared file request for {name} with size {size}: {file_request_data}")


async def extract_all_files(client):
    READ_CHARACTERISTIC_UUID = "1b0d1304-a720-f7e9-46b6-31b601c4fca1"

    def read_notification_handler(sender, data):
        print(f"Notification from READ: {data}")
        read_data[current_command].append(data)

    await client.start_notify(READ_CHARACTERISTIC_UUID, read_notification_handler)
    print("Subscribed to READ notifications.")

    for hex_value, identifier in hex_values_to_write:
        print(f"Requesting data for {identifier}...")
        current_command = identifier
        read_data[current_command] = []

        await client.write_gatt_char(READ_CHARACTERISTIC_UUID, hex_value, response=True)
        print(f"Written to READ characteristic for {identifier}")

        await asyncio.sleep(3)

        file_path = f'{identifier}.bin'
        with open(file_path, 'wb') as file:
            for piece_of_data in read_data[identifier]:
                file.write(piece_of_data)
            print(f"Data for {identifier} written to {file_path}")

    await client.stop_notify(READ_CHARACTERISTIC_UUID)
    print("Unsubscribed from READ notifications.")


async def connect_to_device(device_address):
    print(f"Connecting to {device_address}...")
    async with BleakClient(device_address) as client:
        is_connected = client.is_connected
        if is_connected:
            print(f"Connected to {device_address}.")
            services = client.services
            print("Services discovered:")
            for service in services:
                print(f"Service: {service.uuid}")
                for char in service.characteristics:
                    char_name = CHARACTERISTIC_NAMES.get(char.uuid, "Unknown Characteristic")
                    properties = ', '.join(char.properties)
                    print(f"\tCharacteristic: {char.uuid} ({properties}) = {char_name}")

            while True:
                print("\nOptions:")
                print("1. Read a characteristic")
                print("2. Write to a characteristic")
                print("3. Read all characteristics")
                print("4. List all files")
                print("5. Create FsFileInfo")
                print("6. Create FileRequest")
                print("7. Extract all Files")
                print("8. Exit and disconnect")
                choice = input("Enter your choice: ")

                if choice == '1':
                    char_uuid = input("Enter characteristic UUID to read from: ")
                    try:
                        value = await client.read_gatt_char(char_uuid)
                        print(f"Value: {value}")
                    except Exception as e:
                        print(f"Failed to read characteristic: {e}")

                elif choice == '2':
                    char_uuid = input("Enter characteristic UUID to write to: ")
                    value = input("Enter value to write (as hex string, e.g., '0100'): ")
                    try:
                        value_bytes = bytes.fromhex(value)
                        await client.write_gatt_char(char_uuid, value_bytes, response=True)
                        print("Write successful.")
                    except Exception as e:
                        print(f"Failed to write to characteristic: {e}")

                elif choice == '3':
                    for service in services:
                        print(f"Service: {service.uuid}")
                        for char in service.characteristics:
                            if "read" in char.properties:
                                try:
                                    value = await client.read_gatt_char(char.uuid)
                                    char_name = CHARACTERISTIC_NAMES.get(char.uuid, "Unknown Characteristic")
                                    print(f"\tCharacteristic {char.uuid}: {value} and name = {char_name}")
                                except Exception as e:
                                    print(f"\tFailed to read characteristic {char.uuid}: {e}")
                            else:
                                print(f"\tCharacteristic {char.uuid} does not support reading.")

                elif choice == '4':
                    await list_all_files(client)

                elif choice == '5':
                    await list_all_files(client)
                    display_parsed_file_info()

                elif choice == '6':
                    await list_all_files(client)
                    display_parsed_file_info()
                    process_files_and_create_requests()

                elif choice == '7':
                    print("Extracting all files...")
                    await list_all_files(client)
                    display_parsed_file_info()
                    process_files_and_create_requests()
                    await extract_all_files(client)

                elif choice == '8':
                    print("Disconnecting...")
                    break
                else:
                    print("Invalid choice.")
        else:
            print("Failed to connect.")

    print("Disconnected.")


loop = asyncio.get_event_loop()
loop.run_until_complete(connect_to_device(DEVICE_ADDRESS))
