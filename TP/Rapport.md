# Rapport TP 1: Sécurité des Objets

## Informations sur l'Appareil

- **Nom de l'appareil**: OBU-4872
- **Adresse de l'appareil**: `DD:A1:81:72:F7:F6`

## Objectif

L'objectif principal de ce TP est de retrouver les coordonnées GPS qui sont encodées et stockées à l'intérieur de l'appareil OBU-4872.

## Méthodologie

### Exploration Initiale

J'ai d'abord commencé par me connecter à l'appareil avec mon téléphone en utilisant l'application nRF Connect. Cela m'a permis de découvrir et d'inspecter les différentes caractéristiques BLE disponibles sur l'appareil.

### Lecture des Caractéristiques

Après avoir exploré avec l'application nRF Connect, voici un tableau récapitulatif des services et de leurs caractéristiques accessibles en lecture, ainsi que les actions possibles (lecture, écriture, notification) pour chaque caractéristique.

| Service UUID                           | Characteristic UUID                    | Properties          | Description            |
|----------------------------------------|----------------------------------------|---------------------|------------------------|
| `00001800-0000-1000-8000-00805f9b34fb` | `00002a00-0000-1000-8000-00805f9b34fb` | read, write         | Unknown Characteristic |
|                                        | `00002a01-0000-1000-8000-00805f9b34fb` | read                | Unknown Characteristic |
|                                        | `00002a04-0000-1000-8000-00805f9b34fb` | read                | Unknown Characteristic |
|                                        | `00002aa6-0000-1000-8000-00805f9b34fb` | read                | Unknown Characteristic |
| `00001801-0000-1000-8000-00805f9b34fb` | `00002a05-0000-1000-8000-00805f9b34fb` | indicate            | Unknown Characteristic |
| `0000fe59-0000-1000-8000-00805f9b34fb` | `8ec90003-f315-4f60-9fb8-838830daea50` | write, indicate     | Unknown Characteristic |
| `0000180a-0000-1000-8000-00805f9b34fb` | `00002a29-0000-1000-8000-00805f9b34fb` | read                | Unknown Characteristic |
| `1b0d1200-a720-f7e9-46b6-31b601c4fca1` | `1b0d1201-a720-f7e9-46b6-31b601c4fca1` | read, notify        | TRIP                   |
|                                        | `1b0d1202-a720-f7e9-46b6-31b601c4fca1` | read, notify        | TEMP                   |
|                                        | `1b0d1203-a720-f7e9-46b6-31b601c4fca1` | read, notify        | BATT                   |
|                                        | `1b0d1204-a720-f7e9-46b6-31b601c4fca1` | read, notify        | SOLAR                  |
|                                        | `1b0d1205-a720-f7e9-46b6-31b601c4fca1` | read, notify        | NJR                    |
|                                        | `1b0d1206-a720-f7e9-46b6-31b601c4fca1` | read, notify        | STATUS                 |
|                                        | `1b0d1207-a720-f7e9-46b6-31b601c4fca1` | read, notify        | GPS                    |
|                                        | `1b0d1208-a720-f7e9-46b6-31b601c4fca1` | read                | RESET                  |
| `1b0d1300-a720-f7e9-46b6-31b601c4fca1` | `1b0d1301-a720-f7e9-46b6-31b601c4fca1` | read, notify        | MEM                    |
|                                        | `1b0d1302-a720-f7e9-46b6-31b601c4fca1` | write, notify       | LIST                   |
|                                        | `1b0d1303-a720-f7e9-46b6-31b601c4fca1` | read, notify        | NUM                    |
|                                        | `1b0d1304-a720-f7e9-46b6-31b601c4fca1` | write, notify       | READ                   |
|                                        | `1b0d1305-a720-f7e9-46b6-31b601c4fca1` | write, notify       | SEND                   |
|                                        | `1b0d1306-a720-f7e9-46b6-31b601c4fca1` | write, notify       | DEL                    |
| `1b0d1400-a720-f7e9-46b6-31b601c4fca1` | `1b0d1401-a720-f7e9-46b6-31b601c4fca1` | read, write, notify | MODE                   |
|                                        | `1b0d1402-a720-f7e9-46b6-31b601c4fca1` | read                | FW                     |
|                                        | `1b0d1403-a720-f7e9-46b6-31b601c4fca1` | read                | BL                     |
|                                        | `1b0d1404-a720-f7e9-46b6-31b601c4fca1` | read, write, notify | RESET                  |
|                                        | `1b0d1405-a720-f7e9-46b6-31b601c4fca1` | read, notify        | STATE                  |
|                                        | `1b0d1406-a720-f7e9-46b6-31b601c4fca1` | write               | TIME                   |
|                                        | `1b0d1407-a720-f7e9-46b6-31b601c4fca1` | read, notify        | ACK                    |
|                                        | `1b0d1408-a720-f7e9-46b6-31b601c4fca1` | read, write, notify | PAUSE                  |
|                                        | `1b0d1409-a720-f7e9-46b6-31b601c4fca1` | write               | USER ID                |
|                                        | `1b0d140a-a720-f7e9-46b6-31b601c4fca1` | read, notify        | LOG                    |
|                                        | `1b0d140b-a720-f7e9-46b6-31b601c4fca1` | read, write, notify | EVENT                  |
| `00001500-0000-1000-8000-00805f9b34fb` | `00001503-0000-1000-8000-00805f9b34fb` | read                | NAME                   |
|                                        | `00001504-0000-1000-8000-00805f9b34fb` | read                | COML                   |
|                                        | `00001505-0000-1000-8000-00805f9b34fb` | read                | COMH                   |
|                                        | `00001506-0000-1000-8000-00805f9b34fb` | read                | HWVER                  |

Cette organisation permet une meilleure visibilité des fonctionnalités de chaque caractéristique et facilite la compréhension de leur rôle dans la communication avec l'appareil OBU-4872.

#### Script `Discover.py`

Ce script permet de lister les appareils Bluetooth disponibles à proximité. Il est le point de départ pour établir une connexion avec l'appareil OBU-4872.

#### Script `Read_All.py`

Ce script a pour but de lire toutes les caractéristiques en mode lecture. J'ai utilisé un dictionnaire pour associer à chaque UUID de caractéristique un nom explicite, facilitant ainsi l'identification et l'affichage des valeurs lues.

### Abonnement aux Notifications

En explorant davantage, j'ai identifié deux caractéristiques particulièrement intéressantes : ACK et LOG. J'ai donc créé un script nommé `Subscribe.py` qui s'abonne aux notifications de ces caractéristiques. En envoyant le numéro de fichier (représenté par `\x01\x00`) à la caractéristique LIST, j'ai réussi à recevoir le début du contenu du fichier T0 via des notifications.

#### Notifications Reçues

- **Notification from 58:**
 `bytearray(b'T0\x00\x00\x00\x00\x00\x00P\xf1\x00\x00\xe8\x1c\xc7Z\x10\x7f\xe0j\x8d\x01\x00\x00')`

- **Notification from 100:**
`bytearray(b'\x02\x13\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')`


Suite à l'aide des professeurs, il a été compris que les données précédentes correspondent au `FsFileInfo` qu'il faut parser. La structure attendue est la suivante :

- **Format Little endian :**
  - `name` = 8 octets, exemple : `b'T0\x00\x00\x00\x00\x00'`
  - `size` = 4 octets, format int, exemple : `2143`
  - `hash` = 4 octets, format hex, exemple : `0xfe0be16e`
  - `creationDate` = 8 octets, exemple : `1707603252000`

En parsant les données, on obtient donc : `FsFileInfo(name='T10\x00\x00\x00\x00\x00', size=2143, hash='0xfe0be16e', creationDate=1707732792000)`.

Par la suite, il est nécessaire d'envoyer le nom du fichier, le `chunkId`, et le `numChunk` à la caractéristique READ, qui renvoie ensuite le fichier en plusieurs chunks différents.

### Script `Program.py`

Ce script réalise automatiquement toutes les opérations mentionnées ci-dessus, facilitant ainsi la manipulation et l'extraction des données depuis les appareils BLE Dwilen.

#### Fonctionnalités principales :

Le script `Program.py` est conçu pour extraire automatiquement les données des fichiers suivants présents sur l'appareil cible :

- **T10**
- **T11** 
- **T13** 
- **T4872** 

### Logigramme d'Organisation du Logiciel

![Logigramme d'Organisation du Logiciel](Logigrame.png)

### Validation des Données

La validation de la donnée est cruciale pour assurer l'intégrité et l'exactitude des informations récupérées. Grâce à Monsieur Clement, nous avons découvert la fonction hash dans imHex, un outil qui calcule directement le CRC d'un fichier.